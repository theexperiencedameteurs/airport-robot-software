from ev3dev.ev3 import *

#code for stopping drive motors

def stop_motors():
	ml = LargeMotor('outB')
	mr = LargeMotor('outA')

	ml.stop(stop_action='coast')
	mr.stop(stop_action='coast')
