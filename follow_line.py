from ev3dev.ev3 import *
from time import sleep


M_SPEED_FORWARD = 500
M_SPEED_TURN = 1000


ml = LargeMotor('outB')
mr = LargeMotor('outA')

csl = ColorSensor('in3')
csl.mode = 'COL-COLOR'
csr = ColorSensor('in2')
csr.mode = 'COL-COLOR'

# Follow a line forwards. If start is true the robot will jump to leave the marker it starts on.
def follow_line_fwd(line_color, stop_color, start=True):

	if start:
		ml.run_timed(time_sp=200, speed_sp=500)
		mr.run_timed(time_sp=200, speed_sp=500)
		sleep(0.5)

	cl = cr = 0 # cl = colour from left sensor, cr = colour from right sensor

	while not (cr == stop_color or cl == stop_color):

		cl = int(csl.value())
		cr = int(csr.value())

		if cl != line_color:
			ml.stop(stop_action='coast')
			ml.run_forever(speed_sp=M_SPEED_TURN)
		else:
			ml.stop(stop_action='coast')
			ml.run_forever(speed_sp=M_SPEED_FORWARD)

		if cr != line_color:
			mr.stop(stop_action='coast')
			mr.run_forever(speed_sp=M_SPEED_TURN)
		else:
			mr.stop(stop_action='coast')
			mr.run_forever(speed_sp=M_SPEED_FORWARD)

	ml.stop(stop_action='coast')
	mr.stop(stop_action='coast')

	return 'done'

# The same thing but the robot drives backwards.  Not used.
def follow_line_rev(line_color, stop_color, start=True):

	if start:
		ml.run_timed(time_sp=200, speed_sp= -500)
		mr.run_timed(time_sp=200, speed_sp= -500)
		sleep(0.5)

	cl = cr = 0

	while not (cr == stop_color or cl == stop_color):

		cl = int(csl.value())
		cr = int(csr.value())

		if cl != line_color:
			mr.stop(stop_action='coast')
			mr.run_forever(speed_sp=(-1 * M_SPEED_TURN))
		else:
			mr.stop(stop_action='coast')
			mr.run_forever(speed_sp=(-1 * M_SPEED_FORWARD))

		if cr != line_color:
			ml.stop(stop_action='coast')
			ml.run_forever(speed_sp=(-1 * M_SPEED_TURN))
		else:
			ml.stop(stop_action='coast')
			ml.run_forever(speed_sp=(-1 * M_SPEED_FORWARD))

	ml.stop(stop_action='brake')
	mr.stop(stop_action='brake')

	return 'done'