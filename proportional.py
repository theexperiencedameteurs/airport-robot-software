#!/usr/bin/python3

import ev3dev.ev3 as ev3

#alternative line follower (not in use)

colMin = 0 	#value when over color of line
colMax = 100	#value when over background
colRange = colMax - colMin

speedMin = 0	#min motor speed. make lower (negative) if the line is bendy
speedMax = 100 	#max motor speed. make lower if line following is jerky
speedRange = speedMax - speedMin


#setting up motors and sensors
cl1 = ev3.ColorSensor('in3')
cl2 = ev3.ColorSensor('in2')

ml = ev3.LargeMotor('outB')
mr = ev3.LargeMotor('outA')

#set color sensor 2 to reflect mode and 1 to Color detection mode 

cl1.mode = 'COL-COLOR'
cl2.mode = 'COL-REFLECT'

while cl1.color != 4: #while not yellow
	colVal = cl2.value()
	speed = (colVal - colMin) * (speedRange / colRange)
	ml.stop(stop_action='coast') #stops motors
	mr.stop(stop_action='coast')
	ml.run_forever(speed_sp=(speedMin + speed)*10)
	mr.run_forever(speed_sp=(speedMax - speed)*10)

ml.stop('brake') # stops motors when the loop finishes.
mr.stop('brake')
