from flask import Flask, request
import follow_line
import stop_motors
import turn
#from stop import *
import ev3dev.ev3 as ev3

app = Flask(__name__)

noSteps = 0 # Store the number of steps the robot has taken
side = 'n' # Store the side of the airport the robot is on. 'n' is neither

@app.route('/test/')
def test():
	#web server test
	return "<span style='font-family:sans-serif;font-size:24px'>test</span>"

@app.route('/follow/')
def follow():
	#follow black line
	global noSteps

	try:
		follow_line.follow_line_fwd(1, 4)
		noSteps += 1
	except Exception as e:
		return str(e)

	return 'done'

@app.route('/follown/')
def follown2():
	n = request.args.get('n', '')
	print(follown(n))
	return 'done'

def follown(n):
	# Get the robot to move n steps
	global noSteps

	try:
		if n != '':
			for i in range(int(n)):
				print(i, '/', n)
				follow_line.follow_line_fwd(1, 4)
				noSteps += 1
			return 'done'
		else:
			return "no 'n' parameter"
	except Exception as e:
		return str(e)

@app.route('/stop/')
def stop():
	#emergency stop url

	stop_motors.stop_motors()

	return 'done'

@app.route('/getsteps/')
def getsteps():
	# Get the number of steps the robot has taken

	global noSteps

	return str(noSteps)

@app.route('/resetsteps/')
def resetsteps():
	# Reset the number of steps

	global noSteps

	noSteps = 0

	return 'done'

@app.route('/goto/')
def goto():
	#move a certain number of yellow lines along the black line and then turn.
	global side
	params = request.args.get('l','')
	if params != '':
		n_steps = params[0]
		direction = params[1]
		follown(n_steps)
		turn.turn(direction,5,start=True)
		follow_line.follow_line_fwd(5,4,start=False)
		turn.turn('l', 5, start=True)
		side = direction
		return 'done'	
	else:
		return 'no parameters' , 400

@app.route('/tostart/')
def tostart():
	# Return the robot to the starting point
	global side
	follow_line.follow_line_fwd(5,4,start=False)
	if side.upper() == 'L':
		turn.turn('r', 1, start=True)
	elif side.upper() == 'R':
		turn.turn('l', 1, start=True)
	else:
		return 'invalid direction'
	follow_line.follow_line_fwd(1, 4, start=False)
	for i in range(noSteps-2):
		follow_line.follow_line_fwd(1, 4)
	follow_line.follow_line_fwd(1, 2, start=True)
	turn.turn('l', 1, start=True, start2=True)
	resetsteps()
	return 'done'

@app.route('/rev/')
def rev():
	
	params = request.args.get('l','')
	if params != '':
		direction = params[0]
		n_steps = params[1]
		for i in n_steps:
			follow_line_rev(1,4)
		return 'done'
	else:
		return 'no parameters' , 400

@app.route('/armup/')
def armup():
	#luggage arm commands
	arm = ev3.MediumMotor('outC')
	arm.run_timed(speed_sp=90,time_sp=1000)
	return 'done' 
@app.route('/armdown/')
def armdown():
	arm = ev3.MediumMotor('outC')
	arm.run_timed(speed_sp=-90,time_sp=1000)
	return 'done'
if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=False)
