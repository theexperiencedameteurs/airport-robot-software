from ev3dev.ev3 import *
from time import sleep

M_SPEED_TURN = 200

ml = LargeMotor('outB')
mr = LargeMotor('outA')

csl = ColorSensor('in3')
csl.mode = 'COL-COLOR'
csr = ColorSensor('in2')
csr.mode = 'COL-COLOR'

# Turn the robot either left (d='l') or right (d='r'). If start=True, The robot will move forwards before starting to turn. If start2=True, the robot will turn slightly before starting to check for the line
def turn(d, stop_color, start=True, start2=False):
	# For left turn
	if d.upper() == 'L':

		if start:
			ml.run_timed(time_sp=200, speed_sp= 500)
			mr.run_timed(time_sp=200, speed_sp= 500)
			sleep(0.5)

		if start2:
			ml.run_timed(time_sp=400, speed_sp= -500)
			mr.run_timed(time_sp=400, speed_sp=  500)
			sleep(0.5)

		cl = cr = 0

		while not cr == stop_color: # Turn until the robot hits the line

			cl = int(csl.value())
			cr = int(csr.value())

			ml.run_forever(speed_sp=(-1 * M_SPEED_TURN))
			mr.run_forever(speed_sp=M_SPEED_TURN)

		ml.stop(stop_action='brake') # Brake rather than coast so the robot does not turn further than it needs to
		mr.stop(stop_action='brake')

	# The same but for turning right
	elif d.upper() == 'R':

		if start:
			ml.run_timed(time_sp=200, speed_sp= 500)
			mr.run_timed(time_sp=200, speed_sp= 500)
			sleep(0.5)

		if start2:
			ml.run_timed(time_sp=400, speed_sp=  500)
			mr.run_timed(time_sp=400, speed_sp= -500)
			sleep(0.5)

		cl = cr = 0

		while not cl == stop_color:

			cl = int(csl.value())
			cr = int(csr.value())

			ml.run_forever(speed_sp=M_SPEED_TURN)
			mr.run_forever(speed_sp=(-1 * M_SPEED_TURN))

		ml.stop(stop_action='brake')
		mr.stop(stop_action='brake')
	else:
		return 'invalid direction'